package sh.aios.demodevoxx.service

import io.micrometer.core.annotation.Timed
import io.micrometer.core.instrument.Gauge
import io.micrometer.core.instrument.MeterRegistry
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class DepartementService(
    private val meterRegistry: MeterRegistry
) {
    private val restTemplate: RestTemplate = RestTemplate()

    @Timed(value = "gouv.api.departement.time", description = "time for call api gouv departement")
    fun getDepartements(departementCode: String?): Array<Departement> {
        val time = System.currentTimeMillis()

        val response = restTemplate.getForEntity(
            "https://geo.api.gouv.fr/departements?fields=nom,code,codeRegion${departementCode?.let { "&code=$it" } ?: ""}",
            Array<Departement>::class.java
        )

        departementCode?.apply {
            Gauge.builder("gouv.api.departement") { (System.currentTimeMillis() - time) / 1000 }
                .tag("departementCode", this)
                .register(meterRegistry)
        }

        return response.body ?: emptyArray()
    }
}

data class Departement(
    val nom: String,
    val code: String,
    val codeRegion: String
)