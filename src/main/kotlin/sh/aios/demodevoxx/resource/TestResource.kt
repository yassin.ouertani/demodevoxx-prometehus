package sh.aios.demodevoxx.resource

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import sh.aios.demodevoxx.service.DepartementService

@RestController
class TestResource(
    private val departementService: DepartementService
) {

    @GetMapping("/hello")
    fun hello(
        @RequestParam showError: Boolean = false
    ) = if (showError) throw BadRequestException("Error") else "Hello"

    @GetMapping("/departements")
    fun departements(
        @RequestParam departementCode: String?
    ) = departementService.getDepartements(departementCode)
}