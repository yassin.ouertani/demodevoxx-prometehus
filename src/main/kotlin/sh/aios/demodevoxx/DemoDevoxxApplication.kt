package sh.aios.demodevoxx

import io.micrometer.core.aop.TimedAspect
import io.micrometer.core.instrument.MeterRegistry
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@SpringBootApplication
class DemoDevoxxApplication

@Configuration
class Configuration {
	@Bean
	fun timedAspect(registry: MeterRegistry): TimedAspect {
		return TimedAspect(registry)
	}
}

fun main(args: Array<String>) {
	runApplication<DemoDevoxxApplication>(*args)
}
